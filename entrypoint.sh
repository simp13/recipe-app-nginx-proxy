#!/bin/sh

set -e # run in execution when fail show failure great for debug

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;' # run nginx in foreground to show logs in docker outputs
